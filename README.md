A customer arrival simulator for a food court which consists of a reception queue and multiple food-serving queues.

The final performance achieved by the program: Approximately 30,000 iterations per second, 10m customers in 5 minutes.
