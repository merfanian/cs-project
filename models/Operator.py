from abc import ABC
from models.Queue import CustomerQueue
from models.Customers import Customer
import numpy.random as rnd


class Operator(ABC):

    def __init__(self, working_queue: CustomerQueue, service_rate, rnd_generator: rnd.Generator):
        super().__init__()
        self.__working_queue = working_queue

        self.service_rate = service_rate

        self.__remaining_service_time = 0
        self.__current_customer: Customer = None
        self.__service_times = rnd_generator.exponential(
            1/self.service_rate, 10000100).astype(int)
        self.__current_service_index = -1

    @property
    def working_queue(self):
        return self.__working_queue

    @property
    def current_customer(self):
        return self.__current_customer

    @property
    def remaining_service_time(self):
        return self.__remaining_service_time

    @property
    def remaining_time_or_inf(self):
        if self.current_customer:
            answer = min(self.remaining_service_time,
                         self.current_customer.remaining_tiredness)
            return answer if answer > 0 else 99999

        return 99999

    @property
    def is_free(self):
        return self.remaining_service_time <= 0

    def pass_turn(self, step=1):
        self.__remaining_service_time -= step

        while (self.__remaining_service_time <= 0
               or self.current_customer and self.current_customer.is_tired):
            if self.current_customer:
                if not self.current_customer.is_tired:
                    self._finish_current_customer()
                self.__current_customer = None

            customer = self.__get_next_customer()
            if customer:
                self.__current_customer = customer
                self.__remaining_service_time = self.__get_next_service_time()
                self._start_current_customer()
            else:
                self.__remaining_service_time = 0
                break

    def _start_current_customer(self):
        pass

    def _finish_current_customer(self):
        pass

    def __get_next_customer(self):
        return self.working_queue.next_customer()

    def __get_next_service_time(self) -> int:
        self.__current_service_index += 1
        return self.__service_times[self.__current_service_index]


class ReceptionOperator(Operator):

    def __init__(
            self,
            working_queue: CustomerQueue,
            service_rate,
            rnd_generator: rnd.Generator,
            service_queues: list[CustomerQueue]):
        super().__init__(working_queue, service_rate, rnd_generator)
        self.service_queues = service_queues

    def _start_current_customer(self):
        super()._start_current_customer()
        self.current_customer.notify_receiption_started()

    def _finish_current_customer(self):
        super()._finish_current_customer()
        self.current_customer.notify_receiption_ended()
        self.service_queues[self.current_customer.desired_category].add_customer(
            self.current_customer)


class ServingOperator(Operator):
    def _start_current_customer(self):
        super()._start_current_customer()
        self.current_customer.notify_serving_started()

    def _finish_current_customer(self):
        super()._finish_current_customer()
        self.current_customer.notify_serving_ended()
