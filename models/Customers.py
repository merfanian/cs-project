import numpy.random as rnd
import models.World as w
from tqdm import tqdm


class Customer:

    def __init__(self, id: int, arrival_time: int, priority: int, desired_category: int,
                 tiredness_limit: float) -> None:
        self.__id = id
        self.__arrival_time = arrival_time
        self.__priority = priority
        self.__desired_category = desired_category
        self.__tiredness_limit = tiredness_limit

        self.receiption_start_time = None
        self.receiption_end_time = None
        self.serving_start_time = None
        self.serving_end_time = None

    @property
    def id(self):
        return self.__id

    @property
    def arrival_time(self):
        return self.__arrival_time

    @property
    def priority(self):
        return self.__priority

    @property
    def desired_category(self):
        return self.__desired_category

    @property
    def tiredness_limit(self):
        return self.__tiredness_limit

    @property
    def remaining_tiredness(self):
        return max(self.tiredness_limit - w.the_world.turn, 0)

    @property
    def is_tired(self):
        return w.the_world.turn - self.arrival_time > self.tiredness_limit

    def notify_receiption_started(self):
        self.receiption_start_time = w.the_world.turn

    def notify_receiption_ended(self):
        self.receiption_end_time = w.the_world.turn

    def notify_serving_started(self):
        self.serving_start_time = w.the_world.turn

    def notify_serving_ended(self):
        self.serving_end_time = w.the_world.turn

    def to_dict(self):
        return {
            'id': self.id,
            'priority': self.priority,
            'desired_category': self.desired_category,
            'tiredness_limit': self.tiredness_limit,
            'arrival_time': self.arrival_time,
            'receiption_start_time': self.receiption_start_time,
            'receiption_end_time': self.receiption_end_time,
            'serving_start_time': self.serving_start_time,
            'serving_end_time': self.serving_end_time,
        }

    def __eq__(self, o: object) -> bool:
        if not isinstance(o, Customer):
            raise NotImplemented()

        return self.id == o.id

    def __lt__(self, o: object) -> bool:
        if not isinstance(o, Customer):
            raise NotImplemented()

        if self.priority > o.priority:
            return True

        return self.id < o.id


DEFAULT_CUSTOMER_COUNT = 10000000


class CustomerBringer:

    def __init__(self, arrival_rate, tiredness_rate, serving_category_count, count=DEFAULT_CUSTOMER_COUNT) -> None:
        self.rnd_generator: rnd.Generator = rnd.default_rng()
        # TODO: As you can see we take inter-arrival times as float and
        # arrival times as integer.This may be wrong.

        self.tiredness_rate = tiredness_rate
        self.serving_category_count = serving_category_count

        self.arrival_rate = arrival_rate
        # Currently it's ok. Not that much memory will be consumed.
        self.arrival_times = (
            self.rnd_generator.exponential(
                1 / arrival_rate, size=count)
            .cumsum()
            .astype(int))
        self.__tiredness_limits = self.rnd_generator.exponential(
            1 / self.tiredness_rate, size=count).astype(int)
        self.__choices = self.rnd_generator.choice(
            self.serving_category_count, size=count)
        self.__priorities = self.rnd_generator.choice(
            5, size=count, p=[0.50, 0.20, 0.15, 0.10, 0.05])

        self.customers: list[Customer] = list()
        self.__progress_bar = tqdm(total=count, mininterval=1)

    def bring_customer(self, turn):
        if not self.has_upcoming_customer or self.arrival_times[self.__next_customer_index] > turn:
            return None

        customer = self.__create_customer()
        self.customers.append(customer)
        self.__progress_bar.update()
        return customer

    @property
    def has_upcoming_customer(self):
        return len(self.customers) < len(self.arrival_times)

    @property
    def next_arrival_time(self):
        return self.arrival_times[self.__next_customer_index] if self.has_upcoming_customer else 9999999999999999

    @property
    def __next_customer_index(self):
        return len(self.customers)

    def __create_customer(self):
        return Customer(
            self.__next_customer_index + 1,
            self.arrival_times[self.__next_customer_index],
            self.__priorities[self.__next_customer_index],
            self.__choices[self.__next_customer_index],
            self.__tiredness_limits[self.__next_customer_index],
        )
