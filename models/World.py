import models.Customers
import numpy.random as rnd
from models.Queue import CustomerQueue
from models.Operator import ReceptionOperator, ServingOperator
import pandas as pd


class World:
    def __init__(
            self,
            reception_rate,
            arrival_rate,
            tiredness_rate,
            section_operators_rates,
            customer_count=models.Customers.DEFAULT_CUSTOMER_COUNT):
        rnd_generator = rnd.default_rng()

        section_count = len(section_operators_rates)

        self.reception_queue = CustomerQueue()
        self.serving_queues = [CustomerQueue() for _ in range(section_count)]

        self.reception_operator = ReceptionOperator(
            self.reception_queue, reception_rate, rnd_generator, self.serving_queues)
        self.sections_operators = [
            [ServingOperator(q, rate, rnd_generator)
             for rate in section_operators_rates[i]]
            for i, q in enumerate(self.serving_queues)]

        self.customer_bringer = models.Customers.CustomerBringer(
            arrival_rate, tiredness_rate, section_count, customer_count)

        self.turn = 0
        self.counter = 0

        self.queues_populations = list()
        self.last_record_turn = 0

    @property
    def operators(self):
        yield self.reception_operator
        for operators in self.sections_operators:
            for operator in operators:
                yield operator

    @property
    def queues(self):
        yield self.reception_queue
        for q in self.serving_queues:
            yield q

    def run(self):
        while (self.customer_bringer.has_upcoming_customer
               or any(not operator.is_free for operator in self.operators)):
            next_step = max(min(
                self.customer_bringer.next_arrival_time - self.turn,
                min(operator.remaining_time_or_inf for operator in self.operators)), 1)
            self.pass_turn(step=next_step)

    def pass_turn(self, step=1):
        self.turn += step

        while True:
            new_customer = self.customer_bringer.bring_customer(self.turn)
            if new_customer:
                self.reception_queue.add_customer(new_customer)
            else:
                break

        for operator in self.operators:
            operator.pass_turn(step=step)

        self.record_statistics()

    def record_statistics(self):
        if self.last_record_turn >> 11 == self.turn >> 11:
            return

        self.queues_populations.append(
            (self.turn,) + tuple(len(q) for q in self.queues))
        self.last_record_turn = self.turn

    def save(self):
        import os
        if not os.path.exists("output"):
            os.mkdir("output")

        customers_df = pd.DataFrame(
            c.to_dict() for c in self.customer_bringer.customers).set_index("id")
        customers_df.to_csv(f"output/{len(customers_df)}_" +
                            f"{self.customer_bringer.arrival_rate}_" +
                            f"{self.reception_operator.service_rate}_" +
                            f"{self.customer_bringer.tiredness_rate}_" +
                            "customers.csv")

        queues_df = pd.DataFrame(self.queues_populations,
                                 columns=["time", "receiption"] + [f"q{i}" for i in range(len(self.serving_queues))]) \
            .set_index("time")
        queues_df.to_csv(f"output/{len(customers_df)}_" +
                         f"{self.customer_bringer.arrival_rate}_" +
                         f"{self.reception_operator.service_rate}_" +
                         f"{self.customer_bringer.tiredness_rate}_" +
                         "queues.csv")


the_world: World = None
