import models.World as w


if __name__ == '__main__':
    first_line = input(
        "Input your parameters or 'default' (or enter) for the default configuration:")
    if first_line == "" or first_line == "default":
        n, lam, mu, alpha = 4, 0.75, 0.7, 0.005
        operators_rates = [[0.1, 0.09], [0.05, 0.01], [0.5], [0.05, 0.09]]
    else:
        n, lam, mu, alpha = tuple(float(s.strip())
                                  for s in first_line.split(","))
        operators_rates = [[float(s.strip())
                            for s in input().split(",")] for _ in range(n)]

    w.the_world = w.World(mu, lam, alpha, operators_rates, 1000000)
    w.the_world.run()
    w.the_world.save()
