from models.Customers import Customer
from collections import deque


def _queue_is_empty(queue):
    return not any(not c.is_tired for c in queue)


class CustomerQueue:

    def __init__(self):
        self.priority_queues: list[deque[Customer]] = \
            [deque() for _ in range(5)]
        self.__count = 0

    @property
    def is_empty(self):
        return all(_queue_is_empty(q) for q in self.priority_queues)

    def add_customer(self, customer: Customer) -> None:
        self.priority_queues[customer.priority].append(customer)
        self.__count += 1

    def next_customer(self) -> Customer:
        if self.__count == 0:
            return None

        for q in reversed(self.priority_queues):
            while len(q) > 0:
                head = q.popleft()
                self.__count -= 1
                if not head.is_tired:
                    return head
        return None

    def __len__(self):
        return sum(sum(1 for c in q if not c.is_tired) for q in self.priority_queues)
